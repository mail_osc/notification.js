/*

	web浏览器 消息通知提醒
	作者：管雷鸣
	开源仓库及文档：https://gitee.com/mail_osc/notification.js
	
*/
var notification = {
	version:'1.1.0.20240224',
	use:true,	//是否使用通知，默认为true，使用。如果不使用，那么就是false，false不再播放声音通知、桌面通知
	//播放提醒，执行提醒
	execute:function(title,text){
		if(!this.use){
			//不使用
			return;
		}
		
		//播放声音
		try{
			this.audio.play();
		}catch(e){
			console.log(e);
		}
		
		if(document.location.protocol != 'https:'){
			console.log('当前使用的不是https请求！只有https请求才可以有浏览器消息通知。这里只是声音通知');
			return;
		}
		
		//是https，那么支持Notification通知，使用通知提醒
		if (window.Notification != null){
			//支持通知
			
			if(Notification.permission === 'granted'){
				var notification = new Notification(title, {
					body: text,
					silent: false	//不播放声音。播放声音交给 notification.audio.play
					//sound:notification.audioPath
					//icon: 'https://res.weiunity.com/kefu/images/head.png'
				});
			}else {
				//未授权，弹出授权提示
				Notification.requestPermission();
			};
		}
	},
	audio:{
		url:'https://res.zvo.cn/kefu/media/voice.mp3', //播放的音频文件路径，格式如： https://res.weiunity.com/kefu/media/voice.mp3 。不设置默认便是  https://res.weiunity.com/kefu/media/voice.mp3
		//url:'http://ai.zvo.cn/textToVoice.mp3?text=您有新消息了&speed=1',
		audioBuffer:null,	//声音文件的音频流，通过url加载远程的音频流
		audioContext : new (window.AudioContext || window.webkitAudioContext || window.mozAudioContext || window.msAudioContext),
		/*
		 * 初始化预加载，将加载远程的mp3文件下载下来。这项应该在socket建立完链接之后在进行下载，不然会提前占用网速，导致socket建立连接过慢
		 * successFunc 加载成功后执行的方法。非必传
		 */
		load:function(successFunc){
			if(this.url == null || this.url.length < 1){
				console.log('已将 notification.audioPath 设为空，将不再出现声音提醒');
				return;
			}
			if(notification.audio.audioBuffer != null){
				//已经加载过了，那直接退出
				return; 
			}
			var xhr = new XMLHttpRequest(); //通过XHR下载音频文件
	        xhr.open('GET', this.url, true);
	        xhr.responseType = 'arraybuffer';
	        xhr.onload = function (e) { //下载完成
	        	notification.audio.audioContext.decodeAudioData(this.response, function (buffer) { //解码成功时的回调函数
	        		notification.audio.audioBuffer = buffer;
					if(typeof(successFunc) == 'function'){
						successFunc();
					}
	        	}, function (e) { //解码出错时的回调函数
	        		console.log('notification.load() Error decoding file', e);
	        	});
	        };
	        xhr.send();
		},
		//进行播放声音
		play:function(){
			if(notification.audio.audioBuffer == null){
				//网络加载音频文件。就不判断是否正在加载中了，多加载几次也无所谓了
				notification.audio.load(function(){
					notification.audio.play();
				});
				return; 
			}
			var audioSource = notification.audio.audioContext.createBufferSource();
			audioSource.buffer = notification.audio.audioBuffer;
			audioSource.connect(notification.audio.audioContext.destination);
			audioSource.start(0); //立即播放
		},
		//设置声音提醒所播放的文字，传入如 你有新消息
		setPlayText:function(text){
			//this.setPlayUrl('https://ai.zvo.cn/textToVoice.mp3?text='+text+'&speed=1');
			//api https://czyt.tech/post/a-free-tts-api/
			this.setPlayUrl('https://dds.dui.ai/runtime/v1/synthesize?voiceId=juan1f&text='+text+'&speed=3&volume=50&audioType=wav');
		},
		//设置声音提醒所播放的音频文件。传入如 https://res.weiunity.com/kefu/media/voice.mp3  如果传入空 '' ，则代表不播放声音提醒
		setPlayUrl:function(url){
			this.url = url;
			//清理就的音频文件数据
			notification.audio.audioBuffer = null; 
			//从网络端缓存mp3过来
			this.load();
		}
	}
}
